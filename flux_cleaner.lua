os = require("os")
component = require("component")

robot = component.robot
inventory = component.inventory_controller

side_down = 0
side_up = 1
side_back = 2
side_forward = 3
side_right = 4
side_left = 5

towards_left = false
towards_right = true

function move (direction, steps)
  for i = 1, steps do
    robot.move(direction)
  end
end

function turn (direction)
  robot.turn(direction)
end

function turnAround ()
  robot.turn(towards_left)
  robot.turn(towards_left)
end

function fillUpFilters ()
  inventory.equip()
  robot.suck(0, 64 - robot.count())
  inventory.equip()
end

function cleanLattice ()
  turn(towards_right)
  robot.use(side_forward)
  turn(towards_left)
end

function emptyCrystals ()
  robot.drop(side_down)
end

function cleaningRoutine ()
  fillUpFilters()
  move(side_up, 4)
  move(side_forward, 3)
  turn(towards_left)
  for i = 1, 12 do
    cleanLattice()
    move(side_forward, 1)
  end
  cleanLattice()
  turnAround()
  move(side_forward, 12)
  turn(towards_right)
  move(side_forward, 3)
  move(side_down, 4)
  turnAround()
  emptyCrystals()
end

function main ()
  while true do
    cleaningRoutine()
    os.sleep(60)
  end
end

main()
