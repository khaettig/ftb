local internet = require("internet")
local shell = require("shell")
local io = require("io")

local DOMAIN = "https://khaettig.de/ftb/"
local REGISTERED_FILES_PATH = "registered_files"

local function updateFile (fileName)
  pcall(shell.execute, "rm " .. fileName)
  shell.execute("wget " .. DOMAIN .. fileName)
end

local function getRegisteredFiles ()
  local file = io.open(REGISTERED_FILES_PATH)
  local names = {}
  for line in file:lines() do
    table.insert(names, line)
  end
  file:close()
  return names
end

local function main ()
  updateFile(REGISTERED_FILES_PATH) 
  local files = getRegisteredFiles()
  for _, file in pairs(files) do
    updateFile(file)
  end
end

main()
