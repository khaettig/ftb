local internet = require("internet")
local io = require("io")
local json = assert(loadfile "/lib/json.lua")()
local os = require("os")
local component = require("component")
 
token = "1116973622:AAHWqJf6lc9Z52MU0iYckmKD6I3Et3GDhxI"
chatId = 101995428
url = string.format("https://api.telegram.org/beta/bot%s/", token)
timeStampFile = "/home/lastUpdate.txt"
craftables = component.me_interface.getCraftables()
 
function hasLabel (craftable, name)
  return craftable.getItemStack().label == name
end
 
function getCraftableItem (name)
  for i, craftable in pairs(craftables) do
    if hasLabel(craftable, name) then return craftable end
  end
end
 
function split (input)
  result = {}
  for s in string.gmatch(input, "([^ ]+)") do
    table.insert(result, s)
  end
  return result
end
 
function getLastTimeStamp ()
  for line in io.lines(timeStampFile) do return tonumber(line) end
end
 
function saveTimeStamp (timeStamp)
  local file = io.open(timeStampFile, "w")
  file:write(timeStamp)
  file:close()
end
 
function receive ()
  raw = ""
  for chunk in internet.request(string.format("%s/%s", url, "getUpdates")) do
    raw = raw .. chunk
  end
  return json.decode(raw)["result"]
end
 
function send (text)
  print("Sending: " .. text)
  data = {['chat_id'] = chatId, ['text'] = text}
  return internet.request(string.format("%s/%s", url, "sendMessage"), data)
end
 
function getLatestUpdates ()
  result = {}
  lastTimeStamp = getLastTimeStamp()
  newTimeStamp = lastTimeStamp
  for key, update in pairs(receive()) do
    if lastTimeStamp < tonumber(update["message"]["date"]) then
      print("Receiving: " .. update["message"]["text"])
      table.insert(result, update["message"]["text"])
      newTimeStamp = update["message"]["date"]
    end
  end
  saveTimeStamp(newTimeStamp)
  return result
end
 
function getItemAmount (item)
  filter = {}
  filter["label"] = item
  request = component.me_interface.getItemsInNetwork(filter)
  for pair in pairs(request) do
    print("what the fuck")
    return pair["size"]
  end
  return 0
end
 
function handleUpdate (update)
  tokens = split(update)
  if tokens[1] == "craft" then
    amount = tonumber(tokens[2])
    item = tokens[3]
    for i, value in pairs(tokens) do
      if i > 3 then item = item .. " " .. value end
    end
    craftableItem = getCraftableItem(item)
    if pcall(craftableItem.request(amount)) then
      send("Successfully requested " .. amount .. " of " .. item .. ".")
    else
      send("An error has occured.")
    end
  end
  if tokens[1] == "status" then
    item = tokens[2]
    for i, value in pairs(tokens) do
      if i > 2 then item = item .. " " .. value end
    end
    send(getItemAmount(item))
  end
end
 
function main ()
  print("Finished loading.")
  while true do
    for _, update in pairs(getLatestUpdates()) do
      handleUpdate(update)
    end
    os.sleep(10)
  end
end
 
main()
