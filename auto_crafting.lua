local os = require("os")
local term = require("term")
local component = require("component")
 
local function getCraftables ()
  return component.me_interface.getCraftables()
end
 
local function hasLabel (craftable, name)
  return craftable.getItemStack().label == name
end
 
local function getCraftableItem (name)
  for i, craftable in pairs(component.me_interface.getCraftables()) do
    if hasLabel(craftable, name) then return craftable end
  end
end
 
print("Starting autocrafting ...")
 
terrasteelIngot = getCraftableItem("Terrasteel Ingot")
 
while true do
  print("Requesting to craft Terrasteel Ingot.")
  terrasteelIngot.request(1)
  for t=1,20 do
    term.write(".")
    os.sleep(60)
  end
  print("")
end
